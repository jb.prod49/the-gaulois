use gaulois;

-- 1/ Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
SELECT NumPotion, LibPotion, Formule, ConstituantPrincipal 
    FROM potion;

-- 2/ Liste des noms des trophées rapportant 3 points. (2 lignes)
SELECT NomCateg 
    FROM categorie 
    WHERE NbPoints = 3;

-- 3/ Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)
SELECT NomVillage 
    FROM village 
    WHERE NbHuttes > 35;

-- 4/ Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)
SELECT NumTrophee 
    FROM trophee 
    WHERE DatePrise BETWEEN '2052-05-01' AND '2052-06-30';

-- 5/ Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT Nom 
    FROM habitant 
    WHERE Nom LIKE 'a%r%';

-- 6/ Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT DISTINCT habitant.NumHab 
    FROM habitant 
    LEFT JOIN absorber ON habitant.NumHab = absorber.NumHab 
    WHERE NumPotion IN (1,3,4);

-- 7/ Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)
SELECT trophee.NumTrophee, trophee.DatePrise, categorie.NomCateg 
    FROM trophee 
    LEFT JOIN categorie ON categorie.CodeCat = trophee.CodeCat;

-- 8/ Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT habitant.Nom 
    FROM habitant 
    LEFT JOIN village ON habitant.NumVillage = village.NumVillage 
    WHERE village.NomVillage = 'Aquilona';

-- 9/ Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)
SELECT DISTINCT habitant.Nom 
    FROM habitant 
    LEFT JOIN trophee ON habitant.NumHab = trophee.NumPreneur 
    LEFT JOIN categorie ON trophee.CodeCat = categorie.CodeCat 
    WHERE categorie.NomCateg = 'Bouclier de Légat';

-- 10/ Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)
SELECT potion.LibPotion 
    FROM potion 
    JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion 
    JOIN habitant ON fabriquer.NumHab = habitant.NumHab
    WHERE habitant.Nom = 'Panoramix';

-- 11/ Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)
SELECT DISTINCT potion.LibPotion 
    FROM potion 
    JOIN absorber ON potion.NumPotion = absorber.NumPotion
    JOIN habitant ON absorber.NumHab = habitant.NumHab
    WHERE habitant.Nom = 'Homéopatix';

-- 12/ Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT DISTINCT habitant.Nom 
    FROM habitant
    JOIN absorber ON habitant.NumHab = absorber.NumHab
    WHERE absorber.NumPotion IN (SELECT NumPotion from fabriquer WHERE NumHab = 3);

-- 13/ Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT DISTINCT habitant.nom 
    FROM habitant
    JOIN absorber ON habitant.NumHab = absorber.NumHab
    WHERE absorber.NumPotion IN (SELECT potion.NumPotion FROM potion 
                                    JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion 
                                    JOIN habitant ON fabriquer.NumHab = habitant.NumHab
                                     WHERE habitant.Nom = 'Amnésix');

-- 14/ Nom des habitants dont la qualité n'est pas renseignée. (2 lignes)
SELECT habitant.Nom 
    FROM habitant
    WHERE NumQualite IS NULL;

-- 15/ Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes)
SELECT habitant.Nom 
    FROM habitant
    JOIN absorber ON habitant.NumHab = absorber.NumHab
    JOIN potion ON absorber.NumPotion = potion.NumPotion
    WHERE potion.LibPotion LIKE 'Potion magique n°1' AND absorber.DateA between '2052-02-01' AND '2052-02-29';

-- 16/ Nom et âge des habitants par ordre alphabétique. (22 lignes)
SELECT habitant.Nom, habitant.Age 
    FROM habitant 
    ORDER BY habitant.Age;

-- 17/ Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)
SELECT resserre.NomResserre, village.NomVillage 
    FROM resserre
    JOIN village ON resserre.NumVillage = village.NumVillage
    ORDER BY resserre.NomResserre;

-- 18/ Nombre d'habitants du village numéro 5. (4)
SELECT COUNT(NumHab) 
    FROM habitant 
    WHERE NumVillage = 5 ;

-- 19/ Nombre de points gagnés par Goudurix. (5)
SELECT SUM(categorie.NbPoints) AS PointsGagnesParGoudurix 
    FROM categorie
    JOIN trophee ON categorie.CodeCat = trophee.CodeCat
    JOIN habitant ON trophee.NumPreneur = habitant.NumHab
    WHERE habitant.nom = 'Goudurix';

-- 20/ Date de première prise de trophée. (03/04/52)
SELECT MIN(DatePrise) AS PremierePriseTrophee 
    FROM trophee;


-- 21/ Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées.(19) 
SELECT SUM(absorber.Quantite) AS nombredelouches 
    FROM absorber
    JOIN potion ON absorber.NumPotion = potion.NumPotion
    WHERE potion.LibPotion = "potion magique n°2";

-- Superficie la plus grande. (895)
SELECT MAX(Superficie) AS superficielaplusgrande FROM resserre;

-- 23/ Nombre d'habitants par village (nom du village, nombre). (7 lignes)
SELECT village.NomVillage, COUNT(habitant.NumHab) AS nombrehabitant 
    FROM village
    JOIN habitant ON village.NumVillage = habitant.NumVillage
    GROUP BY village.NomVillage;

-- 24/ Nombre de trophées par habitant (6 lignes)
SELECT habitant.Nom, COUNT(trophee.NumTrophee) AS nombredetrophees
    FROM habitant
    JOIN trophee ON habitant.NumHab = trophee.NumPreneur
    GROUP BY habitant.nom;

-- 25/ Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)
SELECT province.NomProvince , AVG(habitant.Age) AS moyennedage
    FROM province
    JOIN village ON province.NumProvince = village.NumProvince
    JOIN habitant ON village.NumVillage = habitant.NumVillage
    GROUP BY province.NomProvince;

-- 26/ Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT habitant.Nom, COUNT(DISTINCT absorber.NumPotion)AS nombrepotiondifférentes 
    FROM habitant
    JOIN absorber ON habitant.NumHab = absorber.NumHab
    GROUP BY habitant.nom;

-- 27/ Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)
SELECT habitant.Nom 
    FROM habitant
    JOIN absorber ON habitant.NumHab = absorber.NumHab
    JOIN potion ON absorber.NumPotion = potion.NumPotion
    WHERE absorber.Quantite > 2 AND potion.LibPotion = 'potion zen';

-- 28/ Noms des villages dans lesquels on trouve une resserre (3 lignes)
SELECT village.NomVillage, COUNT(resserre.NumResserre) as nombrederesserre
    FROM village
    JOIN resserre ON village.NumVillage = resserre.NumVillage
    GROUP BY village.NomVillage
    HAVING COUNT(resserre.NumResserre) = 1;

-- 29/ Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT village.NomVillage
    FROM village
    WHERE village.NbHuttes = (SELECT NbHuttes
                                    FROM village
                                    GROUP BY NumVillage
                                    ORDER BY COUNT(*) DESC LIMIT 1);

-- 30/ Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).
SELECT habitant.Nom, COUNT(trophee.NumTrophee) AS nombredetrophees
    FROM habitant
    JOIN trophee ON habitant.NumHab = trophee.NumPreneur
    WHERE habitant.nom <> 'Obélix' 
    GROUP BY habitant.Nom
    HAVING COUNT(trophee.NumTrophee) > (SELECT COUNT(trophee.NumTrophee) as nombredetrophees
                                            FROM trophee 
                                            JOIN habitant ON trophee.NumPreneur = habitant.NumHab
                                            WHERE habitant.Nom = 'Obélix');